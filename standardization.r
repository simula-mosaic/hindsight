# Standardization

standardization <- function(data, feature = 1) {
  
  data_stand <- data
  
  m <<- mean(data)
  sdev <<- sd(data)
  data_stand <<- (data - mean(data))/sd(data)
  
  return(data_stand)
}