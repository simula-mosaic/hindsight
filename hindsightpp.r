# Main function for HINDSIGHT++  

hindsightpp <- function(num_features = 1, nlags = 5, n_timesteps = 1, 
                        prep_option = "normalization",  network_type = 'Vanilla', activation = 3, opt = 2,
                        split = 0.8, valsplit = 0.2, search_type = "Manual", niter = 50, 
                        units1 = 256, units2 = 128, units3 = 64, lr = 0.001, nepochs = 50, bs = 8, nlayers = 2,
                        factors = c(), na_factor = 'None', na_option = 'Omit', verbose = 1) {
  
  setwd(dirname(rstudioapi::getSourceEditorContext()$path))
  
  # Load files/functions
  source("preprocessing.r")
  source("standardization.r")
  source("normalization.r")
  source("time_series.r")
  source("manual_search.r")
  source("bidi_lstm.r")
  source("lstm.r")
  source("random_search.r")
  source("bayesian_search.r")
  source("dl_bidi_lstm_fit_bayes.r")
  source("dllstm_fit_bayes.r")
  source("visualize.r")
  source("reverse_prep.r")

  # Load packages
  packages <- c("tcltk2", "caret", "keras", "tensorflow","devtools", "rBayesianOptimization", "stats", "cluster", "kernlab", "factoextra",
                "lmtest", "imputeTS", "tseries", "Metrics")
  if (length(setdiff(packages, rownames(installed.packages()))) > 0) {
    install.packages(setdiff(packages, rownames(installed.packages())))  
  }
  
  suppressPackageStartupMessages(library(devtools))
  suppressPackageStartupMessages(library(tensorflow))
  suppressPackageStartupMessages(library(caret))
  suppressPackageStartupMessages(library(tcltk))
  suppressPackageStartupMessages(library(rBayesianOptimization))
  suppressPackageStartupMessages(library(stats))
  suppressPackageStartupMessages(library(cluster))
  suppressPackageStartupMessages(library(kernlab))
  suppressPackageStartupMessages(library(factoextra))
  suppressPackageStartupMessages(library(lmtest))
  suppressPackageStartupMessages(library(imputeTS))
  suppressPackageStartupMessages(library(keras))
  suppressPackageStartupMessages(library(tseries))
  suppressPackageStartupMessages(library(Metrics))

  # Check the os type for compatibility reasons
  os = .Platform$OS.type

  # Data Import
  if(os == "unix"){
    files = tk_choose.files()
  }  else if(os == "windows"){
    files = choose.files()
  }

  nfeat <<- num_features
  n_timesteps <<- n_timesteps
  n_timeseries <<- length(files)
  activation <<- activation
  valsplit <<- valsplit
  prep_option <<- prep_option
  flag <<- 0
  
  df_list <- list()
  feature_mat <- list()
  
  for(i in 1:length(files)){
    if(grepl(".csv", files[i])){
      df_list[[i]] <- read.csv(files[i], header = TRUE, sep = ",", dec = ".")
      tolis <<- df_list[[i]]

      if(length(factors) != 0){
        for(k in factors){
          df_list[[i]][[k]] <- factor(df_list[[i]][[k]])
        }
      }
    }
    else if(grepl(".xlsx", files[i])){
      df_list[[i]] = read(files[i])
    }
  }

  #NA masking
  if(na_option == "Omit"){
    for(i in 1:length(files)){
      df_list[[i]] <- na.omit(df_list[[i]])
    }
  } else{
    for(i in 1:length(files)){
      for(j in 1:nfeat){
        if((is.factor(df_list[[i]][[j]]))){
          if(na_factor == "None"){
            temp = df_list[[i]][[j]]
            # Get levels and add "None"
            levels <- levels(temp)
            levels[length(levels) + 1] <- "None"
            # refactor to include "None" as a factor level
            # and replace NA with "None"
            temp <- factor(temp, levels = levels)
            temp[is.na(temp)] <- "None"
            df_list[[i]][[j]] <-  temp
          } else{
            if(na_option == "Kalman filtering"){
              df_list[[i]][[j]] <- na_kalman(df_list[[i]][[j]])
            }
            else if(na_option == "Moving average"){
              df_list[[i]][[j]] <- na_ma(df_list[[i]][[j]])
            }
            else if(na_option == "Mean value"){
              df_list[[i]][[j]] <- na_mean(df_list[[i]][[j]])
            }
            else if(na_option == "Random sample"){
              df_list[[i]][[j]] <- na_random(df_list[[i]][[j]])
            }
            else if(na_option == "Replacement"){
              df_list[[i]][[j]] <- na_replace(df_list[[i]][[j]])
            }
            else if(na_option == "Interpolation"){
              df_list[[i]][[j]] <- na_interpolation(df_list[[i]][[j]])
            }
          }
        }
      }
    }
  }
  
  new_features = 0
  
  for(i in 1:length(files)){
    for(j in 1:nfeat){
      if((i == 1) && (is.factor(df_list[[i]][[j]]))){
        new_features = new_features + nlevels(df_list[[i]][[j]]) - 1
        dmy <- dummyVars("~.", data = df_list[[i]])
        df_list[[i]] <- data.frame(predict(dmy, newdata = df_list[[i]]))
      }
    }
  }

  nfeat = nfeat + new_features
  
  prep_params <<- preprocessing(df_list, nlags = nlags, n_timesteps = n_timesteps, nfeatures = nfeat, split = split, prep_option = prep_option)

  # Hyperparameter Optimization
  if(search_type == "Manual"){
    results = manual_search(nlags = nlags, network_type = network_type, units = nlags,  units1 = units1, units2 = units2, 
                            units3 = units3,  lr = lr, nepochs = nepochs, bs = bs, nlayers = nlayers, opt = opt)
  }
  else if(search_type == "Random"){
    results = random_search(nlags = nlags, network_type = network_type, units = nlags,  units1 = units1, units2 = units2, 
                            units3 = units3,  lr = lr, nepochs = nepochs, bs = bs, nlayers = nlayers, niter = niter, opt = opt)
    
  }
  else if(search_type == "Bayesian"){
    results = bayesian_search(nlags = nlags, network_type = network_type, units = nlags, units1 = units1, units2 = units2, 
                              units3 = units3, lr = lr, nepochs = nepochs, bs = bs, nlayers = nlayers, niter = niter, opt = opt)
  }
  else{
    warning("No valid optimization option was provided")
    return()
  }

  # De-normalize predicted data
  datasets_denormalized <- reverse_prep(prep_params, prep_option)
  yhat_train_back <<- datasets_denormalized[1]
  yhat_val_back <<- datasets_denormalized[2]
  yhat_test_back <<- datasets_denormalized[3]
  
  df_list <<- df_list # For debugging purposes
  
  # Visualization     
  visualize(nlags = nlags, list(as.data.frame(yhat_train_back), as.data.frame(yhat_val_back), as.data.frame(yhat_test_back)), df_list)
  
  rmse_val = list()
  mae_val = list()
  
  rmse_test = list()
  mae_test = list()
  
  n=1
  for (i in 1:dim(y_train)[3]) {
    cat('\n parallel',n)
    for (j in 1:n_timesteps) {
      val = yhat_val_back[[1]]
      test = yhat_test_back[[1]]
      
      rmse_val[n] = rmse(as.numeric(unlist(y_val_back[,n])),unlist(as.vector(val[,n]))) 
      mae_val[n] = mae(as.numeric(unlist(y_val_back[,n])),unlist(as.vector(val[,n])))
      
      rmse_test[n] = rmse(as.numeric(unlist(y_test_back[,n])),unlist(as.vector(test[,n]))) 
      mae_test[n] = mae(as.numeric(unlist(y_test_back[,n])),unlist(as.vector(test[,n])))
      
      cat('\n (val) The error for timestep',j)
      cat("\nRMSE (val): ", as.numeric(rmse_val[n]))
      cat("\nMAE (val): ", as.numeric(mae_val[n]))
      
      cat('\n (test) The error for timestep',j)
      cat("\nRMSE (test): ", as.numeric(rmse_test[n]))
      cat("\nMAE (test): ", as.numeric(mae_test[n]))
    }
    n = n + 1
  }
  
}

# Testing HINDSIGHT++
res = hindsightpp(network_type = "Vanilla", search_type = "Bayesian")
