# Preprocesssing

preprocessing <- function(df, nlags, n_timesteps, nfeatures, split = 0.67, prep_option = "normalization"){
  for(i in 1:nfeatures){
    for(j in 1:length(df)){
      
      # normalization and standardization takes place here.
      if(all(prep_option == "normalization") && (!is.factor(df[[j]][[i]]))){
        temp = normalization(df[[j]][[i]], i)
        if(i == 1){
          if(j == 1){
            maximums <- c(maxim)
            minimums <- c(minim)
          }
          else{
            minimums <- c(minimums, minim)
            maximums <- c(maximums, maxim)
          }
        }
      } else if(all(prep_option == "standardization")){
          temp = standardization(df[[j]][[i]], j)
          if(i == 1){
            if(j == 1){
              means <- c(m)
              sdevs <- c(sdev)
            }
            else{
              means <- c(means, m)
              sdevs <- c(sdevs, sdev)
            }
          }
      } else{
          temp = (df[[j]][[i]])  
      }      
      
      temp = as.data.frame(temp)

      df_train = temp[1:as.integer(dim(as.data.frame(temp))[1]*(split-valsplit)), ]
      df_val = temp[as.integer(dim(temp)[1]*(split-valsplit) + 1):as.integer(dim(as.data.frame(temp))[1]*(split)), ]
      df_test = temp[as.integer(dim(temp)[1]*split + 1):dim(temp)[1],]
      
      
      df_train = time_series(na.omit(as.data.frame((df_train))), nlags + n_timesteps -1, 1)
      df_val = time_series(na.omit(as.data.frame((df_val))), nlags + n_timesteps -1, 1)
      df_test = time_series(na.omit(as.data.frame((df_test))), nlags + n_timesteps -1, 1)
      
      starting_point = ncol(df_train)-n_timesteps+1 
      
      if(i == 1){
        if(j == 1){
          
          X_train <<- as.data.frame(df_train[, 1:starting_point-1])
          y_train <<- as.data.frame(df_train[, starting_point:ncol(df_train)])
          
          X_val <<- as.data.frame(df_val[, 1:starting_point-1])
          y_val <<- as.data.frame(df_val[, starting_point:ncol(df_val)])
          
          X_test <<- as.data.frame(df_test[, 1:starting_point-1])
          y_test <<- as.data.frame(df_test[, starting_point:ncol(df_test)])

        }
        else{
          y_train <<- cbind(y_train, as.data.frame(df_train[, starting_point:ncol(df_train)]))
          y_val <<- cbind(y_val, as.data.frame(df_val[, starting_point:ncol(df_val)]))
          y_test <<- cbind(y_test, as.data.frame(df_test[, starting_point:ncol(df_test)]))

          X_train <<- cbind(X_train, as.data.frame(df_train[, 1:starting_point-1]))
          X_val <<- cbind(X_val, as.data.frame(df_val[, 1:starting_point-1]))
          X_test <<- cbind(X_test, as.data.frame(df_test[, 1:starting_point-1]))
        }
      }
      else{
        
        X_train <<- cbind(X_train, as.data.frame(df_train[, 1:starting_point-1]))
        X_val <<- cbind(X_val, as.data.frame(df_val[, 1:starting_point-1]))
        X_test <<- cbind(X_test, as.data.frame(df_test[, 1:starting_point-1]))
        
      }
    }    
  }
  
  # split the data in training and testing/validation
  X_train <<- matrix(unlist(X_train), ncol = nlags*length(df)*nfeatures, byrow = FALSE)
  X_val <<- matrix(unlist(X_val), ncol = nlags*length(df)*nfeatures, byrow = FALSE)
  X_test <<- matrix(unlist(X_test), ncol = nlags*length(df)*nfeatures, byrow = FALSE)
  X_train <<- array(X_train, dim = c(nrow(X_train), nlags, nfeatures*length(df)))
  X_val <<- array(X_val, dim = c(nrow(X_val), nlags, nfeatures*length(df)))
  X_test <<- array(X_test, dim = c(nrow(X_test), nlags, nfeatures*length(df)))
  
  y_train <<- matrix(unlist(y_train), ncol = n_timesteps*length(df), byrow = FALSE)
  y_val <<- matrix(unlist(y_val), ncol = n_timesteps*length(df), byrow = FALSE)
  y_test <<- matrix(unlist(y_test), ncol = n_timesteps*length(df), byrow = FALSE)
  y_train <<- array(y_train, dim = c(nrow(y_train), n_timesteps, length(df)))
  y_val <<- array(y_val, dim = c(nrow(y_val), n_timesteps, length(df)))
  y_test <<- array(y_test, dim = c(nrow(y_test), n_timesteps, length(df)))
  
  if(prep_option == "normalization"){
    return(list(minimums, maximums))
  }
  else if(prep_option == "Standardization"){
    return(c(means, sdevs))
  }
}

